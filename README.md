This is an implementation of the SMC algorithm. It has been heavily inspired by the POMP package. The package is based on a C++ implementation, which has been wrapped in an R interface, with very similar interface. This makes it easy to prototype your model in R and when you need more performance reimplement the same model in C++ (based on Rcpp). 

For usage details see the [vignette](https://epidemics-r.gitlab.io/simplersmc/simpleRSMC_usage.html).
