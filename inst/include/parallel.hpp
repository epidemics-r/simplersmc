#pragma once

#include <atomic>
#include <vector>

#include <Rcpp.h>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wignored-attributes"
#include <RcppEigen.h>
#pragma GCC diagnostic pop
#include <RcppParallel.h>

#include <smc.hpp>

namespace smc {

template <typename S, typename D, typename URNG, typename F>
class BatchWorker : public RcppParallel::Worker {
public:
  const size_t nparticles;
  const Eigen::MatrixXd &params;
  const std::vector<D> &data;

  const std::vector<int> &times;
  const int t0;

  F &build_function;

  bool history;

  std::vector<double> logLikelihoods;
  std::vector<std::vector<S>> particles;
  std::vector<std::vector<D>> measurements;

  std::atomic<size_t> seed{0};

  BatchWorker() = delete;
  BatchWorker(size_t nparticles, const Eigen::MatrixXd &params,
              const std::vector<D> &data, const std::vector<int> &times, int t0,
              F &build_function, bool history)
      : nparticles(nparticles), params(params), data(data), times(times),
        t0(t0), build_function(build_function), history(history),
        logLikelihoods(params.rows()) {
    if (history) {
      particles.resize(params.rows());
      measurements.resize(params.rows());
    }
  }

  void operator()(size_t begin, size_t end) {
    static thread_local URNG generator(++seed +
                                       static_cast<long unsigned int>(time(0)));
    auto model = build_function(generator);
    for (size_t k = begin; k < end; ++k) {
      model.smc(nparticles, params.row(k), data, times, t0, history);
      if (history && std::isfinite(model.logLikelihood)) {
        auto parts = model.get_particles(1);
        if (parts.size() > 0) {
          auto meas = model.get_measurements(parts, params.row(k));
          particles[k] = parts[0];
          measurements[k] = meas[0];
        }
      }
      logLikelihoods[k] = model.logLikelihood;
    }
  }
};

template <typename S, typename D, typename URNG = std::default_random_engine,
          typename F>
std::vector<double> batch_smc(size_t nparticles, const Eigen::MatrixXd &params,
                              const std::vector<D> &data,
                              const std::vector<int> &times, int t0,
                              F &build_function) {
  BatchWorker<S, D, URNG, F> bworker(nparticles, params, data, times, t0,
                                     build_function, false);
  RcppParallel::parallelFor(0, params.rows(), bworker);
  return bworker.logLikelihoods;
}

template <typename S, typename D, typename URNG = std::default_random_engine,
          typename F>
std::vector<Rcpp::List>
batch_smc_history(size_t nparticles, const Eigen::MatrixXd &params,
                  const std::vector<D> &data, const std::vector<int> &times,
                  int t0, F &build_function) {
  BatchWorker<S, D, URNG, F> bworker(nparticles, params, data, times, t0,
                                     build_function, true);
  RcppParallel::parallelFor(0, params.rows(), bworker);

  std::vector<Rcpp::List> result;
  for (size_t i = 0; i < bworker.logLikelihoods.size(); ++i) {
    Rcpp::List rstate(0);
    rstate["logLikelihood"] = bworker.logLikelihoods[i];
    if (std::isfinite(bworker.logLikelihoods[i])) {
      rstate["particles"] = Rcpp::wrap(bworker.particles[i]);
      // Should only be the case if model has no rmeasure function
      if (bworker.measurements[i].size() > 0) {
        rstate["measurements"] = Rcpp::wrap(bworker.measurements[i]);
      }
    }
    result.push_back(Rcpp::clone(rstate));
  }
  return result;
}

}; // namespace smc
