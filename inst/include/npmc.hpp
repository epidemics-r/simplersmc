// Mean and covariance
//
// rmvnorm
// dmvnorm http://blog.sarantop.com/notes/mvn (adapt for log version)
//
// Similar object to simpleRSMC that to build takes a prior (draw, pdf) and
// likelihood Also allow parallelized likelihood and prior Use log friendly
// versions (smc.hpp) to sum and normalise weights
//
// Ideally add adaptive step that will increase initial samples till we get a
// working first posterior.. similar with particle numbers
//
#ifndef NPMC_HPP
#define NPMC_HPP

#ifndef FREESTANDING
#include <Rcpp.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wignored-attributes"
#include <RcppEigen.h>
#pragma GCC diagnostic pop
#else
#include <Eigen/Dense>
#endif

#include <chrono>
#include <random>

#include "sample.hpp"
#include "smc.hpp"

namespace npmc {

template <class URNG>
Eigen::VectorXd rmvnorm(const Eigen::VectorXd &mu, const Eigen::MatrixXd &cov,
                        URNG &generator) {
  static std::normal_distribution<double> rnorm(0.0, 1.0);
  Eigen::VectorXd v(mu.size());
  for (auto i = 0; i < mu.size(); ++i) v(i) = rnorm(generator);

  Eigen::MatrixXd transform;
  Eigen::LLT<Eigen::MatrixXd> cholSolver(cov);
  // We can only use the cholesky decomposition if
  // the covariance matrix is symmetric, pos-definite.
  // But a covariance matrix might be pos-semi-definite.
  // In that case, we'll go to an EigenSolver
  if (cholSolver.info() == Eigen::Success) {
    // Use cholesky solver
    transform = cholSolver.matrixL();
  } else {
    auto eigenSolver = Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd>(cov);
    transform = eigenSolver.eigenvectors() *
                eigenSolver.eigenvalues().cwiseMax(0).cwiseSqrt().asDiagonal();
  }

  return mu + transform * v;
}

double dmvnorm(const Eigen::VectorXd &x, const Eigen::VectorXd &mu,
               const Eigen::MatrixXd &sigma) {
  double n = x.rows();
  //double sqrt2pi = std::sqrt(2 * M_PI);
  double lsqrt2pi = 0.5 * log(2 * M_PI);
  double quadform = (x - mu).transpose() * sigma.inverse() * (x - mu);
  //double norm = std::pow(sqrt2pi, -n) * std::pow(sigma.determinant(), -0.5);
  double lnorm = -n * lsqrt2pi - 0.5 * log(sigma.determinant());

  //return log(norm) + (-0.5 * quadform);
  return lnorm + (-0.5 * quadform);
}

std::vector<std::size_t> sort_permutation(const Eigen::ArrayXd &vec) {
  std::vector<std::size_t> p(vec.size());
  std::iota(p.begin(), p.end(), 0);
  std::sort(p.begin(), p.end(),
            [&](std::size_t i, std::size_t j) { return vec(i) < vec(j); });
  return p;
}

template <typename T>
T apply_permutation(T &&vec, const std::vector<std::size_t> &p) {
  std::vector<bool> done(vec.size());
  for (auto i = 0; i < vec.size(); ++i) {
    if (done[i]) {
      continue;
    }
    done[i] = true;
    std::size_t prev_j = i;
    std::size_t j = p[i];
    while (i != j) {
      std::swap(vec[prev_j], vec[j]);
      done[j] = true;
      prev_j = j;
      j = p[j];
    }
  }
  return vec;
}

using Samples = simplersmc::Samples<Eigen::VectorXd>;

/**
 *
 * P is the parameter vector. The rmvnorm is likely to put some constraints
 * on the permissable types
 */
template <typename D>
class Model {
 public:
  Model() {
    rpriorN = [this](size_t n) {
      std::vector<Eigen::VectorXd> params(n);
      for (size_t i = 0; i < n; ++i) {
        params[i] = this->rprior();
      }
      return params;
    };

    dpriorN = [this](size_t n, std::vector<Eigen::VectorXd> params) {
      Eigen::ArrayXd lweights(n);
      for (size_t i = 0; i < n; ++i) {
        lweights[i] = this->dprior(params[i]);
      }
      return lweights;
    };

    dmeasureN = [this](size_t n, D data, std::vector<Eigen::VectorXd> params) {
      Eigen::ArrayXd lweights(n);
      for (size_t i = 0; i < n; ++i) {
        lweights[i] = this->dmeasure(data, params[i]);
      }
      return lweights;
    };

    auto seed = std::chrono::duration_cast<std::chrono::milliseconds>(
                    std::chrono::system_clock::now().time_since_epoch())
                    .count();
    this->generator.seed(seed);
  }

  Samples step(size_t nsamples, D data, size_t nclipping = 0,
               bool first = false) {
    Samples samples;

    Eigen::ArrayXd lclipWeights;
    if (first) {
      //    - pick samples (rpriorN)
      samples.samples = this->rpriorN(nsamples);
      //    - Calculate weights (dmeasureN)
      samples.lweights = this->dmeasureN(nsamples, data, samples.samples);
      initialised = true;
    } else {
      samples.samples.resize(nsamples);
      //    - Draw samples using rmvnorm
      Eigen::ArrayXd dq(nsamples);
      for (size_t i = 0; i < nsamples; ++i) {
        samples.samples[i] = rmvnorm(mvMeans, mvCovariance, generator);
        dq[i] = dmvnorm(samples.samples[i], mvMeans, mvCovariance);
      }
      auto dpriorsFull = dpriorN(nsamples, samples.samples);
      Eigen::ArrayXd dpriors(nsamples);

      // - Throw out samples that have dprior == -Inf before we call
      //   the potentially costly dmeasureN
      size_t j = 0;
      for (size_t i = 0; i < nsamples; ++i) {
        if (!std::isfinite(dpriorsFull[i])) continue;
        dpriors[j] = dpriorsFull[i];
        // j is always <= i, so this should be safe
        if (j != i) {
          samples.samples[j] = samples.samples[i];
          dq[j] = dq[i];
        }
        j++;
      }
      samples.samples.resize(j);
      dpriors.resize(j);
      dq.resize(j);

      // - Calculate weights (dmeasureN)
      samples.lweights =
          this->dmeasureN(samples.samples.size(), data, samples.samples);
      samples.lweights = samples.lweights + dpriors - dq;
    }

    //size_t mx = floor(sqrt(samples.samples.size()));
    size_t mx = floor(samples.samples.size()/10);
    if (nclipping == 0 || nclipping > mx) nclipping = mx;

    // - Sort samples (and lweights) according and clipweights
    //     -
    //     https://stackoverflow.com/questions/17074324/how-can-i-sort-two-vectors-in-the-same-way-with-criteria-that-uses-only-one-of
    //         - Apply_permutation could rely on std::move to make it act in
    //         place
    //    - Set lclipw = lweights (no need for dpriorN and dmvnorm yet)
    for (auto i = 0; i < samples.lweights.size(); ++i) {
      if (!std::isfinite(samples.lweights(i))) samples.lweights(i) = log(0);
    }

    lclipWeights = samples.lweights;

    auto sv = sort_permutation(lclipWeights);
    samples.samples = apply_permutation(std::move(samples.samples), sv);
    samples.lweights = apply_permutation(std::move(samples.lweights), sv);
    lclipWeights = apply_permutation(std::move(lclipWeights), sv);
    // - Find threshold weight
    auto threshold = lclipWeights(lclipWeights.size() - 1 - nclipping);
    // - Set all clipweights greater than limit to threshold (replace_if?)
    for (size_t i = 0; i < lclipWeights.size(); ++i) {
      if (lclipWeights(i) > threshold) lclipWeights(i) = threshold;
    }

    // We use @kobl_bayesian_2019 slight change to the original algorithm by
    // resampling here and then call upDateMV with the new samples
    // Note that after resampling, lweight will be 0, so the normalised weights is
    // exp(0)*1.0/nsamples
    samples.lweights = lclipWeights;
    //Rcpp::Rcout << lclipWeights << " " << nsamples << std::endl;
    samples.ids.resize(nsamples);
    samples = smc::resampling_systematic(std::move(samples), nsamples, generator);

    // Copy the samples into the new order
    samples.step();

    //samples = smc::resampling_multinom(samples, nsamples, generator);
    //Rcpp::Rcout << 1.0/nsamples*samples.lweights.exp() << " " << nsamples << std::endl;
    updateMV(1.0/nsamples*samples.lweights.exp(), samples.samples);

    return samples;

    /*auto weights = smc::relativeLogWeight(lclipWeights);
    auto clippedWeights = weights / weights.sum();

    // - Update mean and covariance (based on normalised clipped weights)
    updateMV(clippedWeights, samples.samples);

    return samples;*/
  }

  std::vector<Eigen::VectorXd> inference(size_t niterations, size_t nsamples,
                                         D data) {
    auto smpls = step(nsamples, data, 0, !initialised);
    for (auto i = 0; i < niterations - 2; ++i)
      smpls = step(nsamples, data, 0, false);
  
    // We do a final step essentially without clipping (nclipping = 1)
    // This makes sure the final samples are all weighted towards the 
    // likelihood. According to the manuscript this is not needed, but
    // I do believe it is needed!
    smpls = step(nsamples, data, 1, false);
    return smpls.samples;
  }

  ~Model() {}

  // Prior
  std::function<Eigen::VectorXd()> rprior;
  std::function<std::vector<Eigen::VectorXd>(size_t n)> rpriorN;

  std::function<double(Eigen::VectorXd)> dprior;
  std::function<Eigen::ArrayXd(size_t n, std::vector<Eigen::VectorXd>)> dpriorN;

  // Likelihood
  std::function<double(D, Eigen::VectorXd)> dmeasure;
  std::function<Eigen::ArrayXd(size_t n, D, std::vector<Eigen::VectorXd>)>
      dmeasureN;

 private:
  std::default_random_engine generator;
  Eigen::VectorXd mvMeans;
  Eigen::MatrixXd mvCovariance;

  bool initialised = false;

  void updateMV(Eigen::ArrayXd weights, std::vector<Eigen::VectorXd> params) {
    mvMeans = Eigen::VectorXd::Zero(params[0].size());
    mvCovariance = Eigen::MatrixXd::Zero(params[0].size(), params[0].size());

    for (auto i = 0; i < weights.size(); ++i) {
      mvMeans += weights[i] * params[i];
    }

    for (auto i = 0; i < weights.size(); ++i) {
      // Update cov
      mvCovariance += weights[i] * (params[i] - mvMeans) *
                      (params[i] - mvMeans).transpose();
    }
  }
};  // namespace npmc
}  // namespace npmc

#endif

