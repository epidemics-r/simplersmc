#pragma once

#include <array>
#include <random>

#ifndef FREESTANDING
#include <Rcpp.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wignored-attributes"
#include <RcppEigen.h>
#pragma GCC diagnostic pop
#else
#include <Eigen/Dense>
namespace Rcpp {
void warning(const std::string &str) { std::cout << str << std::endl; }
}; // namespace Rcpp
#endif

namespace distribution {

constexpr double mininf = -std::numeric_limits<double>::infinity();
constexpr double pi = 3.14159265358979323846;

constexpr std::array<double, 126> logfact = {0,
                                             0,
                                             0.69314718055994529,
                                             1.791759469228055,
                                             3.1780538303479458,
                                             4.7874917427820458,
                                             6.5792512120101012,
                                             8.5251613610654147,
                                             10.604602902745251,
                                             12.801827480081469,
                                             15.104412573075516,
                                             17.502307845873887,
                                             19.987214495661885,
                                             22.552163853123425,
                                             25.19122118273868,
                                             27.89927138384089,
                                             30.671860106080672,
                                             33.505073450136891,
                                             36.395445208033053,
                                             39.339884187199495,
                                             42.335616460753485,
                                             45.380138898476908,
                                             48.471181351835227,
                                             51.606675567764377,
                                             54.784729398112319,
                                             58.003605222980518,
                                             61.261701761002001,
                                             64.557538627006338,
                                             67.88974313718154,
                                             71.257038967168015,
                                             74.658236348830158,
                                             78.092223553315307,
                                             81.557959456115043,
                                             85.054467017581516,
                                             88.580827542197682,
                                             92.136175603687093,
                                             95.719694542143202,
                                             99.330612454787428,
                                             102.96819861451381,
                                             106.63176026064346,
                                             110.32063971475739,
                                             114.03421178146171,
                                             117.77188139974507,
                                             121.53308151543864,
                                             125.3172711493569,
                                             129.12393363912722,
                                             132.95257503561632,
                                             136.80272263732635,
                                             140.67392364823425,
                                             144.5657439463449,
                                             148.47776695177302,
                                             152.40959258449735,
                                             156.3608363030788,
                                             160.3311282166309,
                                             164.32011226319517,
                                             168.32744544842765,
                                             172.35279713916279,
                                             176.39584840699735,
                                             180.45629141754378,
                                             184.53382886144948,
                                             188.6281734236716,
                                             192.7390472878449,
                                             196.86618167289001,
                                             201.00931639928152,
                                             205.1681994826412,
                                             209.34258675253685,
                                             213.53224149456327,
                                             217.73693411395422,
                                             221.95644181913033,
                                             226.1905483237276,
                                             230.43904356577696,
                                             234.70172344281826,
                                             238.97838956183432,
                                             243.26884900298271,
                                             247.57291409618688,
                                             251.89040220972319,
                                             256.22113555000954,
                                             260.56494097186322,
                                             264.92164979855278,
                                             269.29109765101981,
                                             273.67312428569369,
                                             278.06757344036612,
                                             282.4742926876304,
                                             286.89313329542699,
                                             291.32395009427029,
                                             295.76660135076065,
                                             300.22094864701415,
                                             304.68685676566872,
                                             309.1641935801469,
                                             313.65282994987905,
                                             318.1526396202093,
                                             322.66349912672615,
                                             327.1852877037752,
                                             331.71788719692847,
                                             336.26118197919845,
                                             340.81505887079902,
                                             345.37940706226686,
                                             349.95411804077025,
                                             354.53908551944079,
                                             359.1342053695754,
                                             363.73937555556347,
                                             368.35449607240474,
                                             372.97946888568902,
                                             377.61419787391867,
                                             382.25858877306001,
                                             386.91254912321756,
                                             391.57598821732961,
                                             396.24881705179155,
                                             400.93094827891576,
                                             405.6222961611449,
                                             410.32277652693733,
                                             415.03230672824964,
                                             419.75080559954472,
                                             424.47819341825709,
                                             429.21439186665157,
                                             433.95932399501481,
                                             438.71291418612117,
                                             443.47508812091894,
                                             448.24577274538461,
                                             453.02489623849613,
                                             457.81238798127816,
                                             462.60817852687489,
                                             467.4121995716082,
                                             472.22438392698058,
                                             477.04466549258564,
                                             481.87297922988796};

template <typename T> inline double logfactorial(T k) {
  //  Using  Srinivasa Ramanujan (Ramanujan 1988)
  //  See bottom: https://en.wikipedia.org/wiki/Stirling%27s_approximation
  if (k < 0)
    return -mininf;
  if (k < logfact.size()) {
    // Use lookup table
    return logfact[k];
  }
  constexpr double a1 = 1.0 / 30.0;
  return k * log(k) - k + (log(pow(k, 3) * 8 + pow(k, 2) * 4 + k + a1)) / 6.0 +
         log(distribution::pi) / 2.0;
}
// TODO: Specialise when called with positive integer (to use logfactorial)
template <typename T> inline double loggamma(T k) {
  return lgamma(k);
  /*if (k > 1)
    return logfactorial(k - 1);
  else
    return lgamma(k);*/
}

template <typename T> inline double log_binom_coeff(T n, T k) {
  return logfactorial(n) - (logfactorial(k) + logfactorial(n - k));
}

template <typename T>
inline bool between(const T value, const T lower, const T upper) {
  return (value >= lower && value <= upper);
}

template <typename URNG>
double runif(const double mn, const double mx, URNG &generator) {
  std::uniform_real_distribution<double> distribution(mn, mx);
  return distribution(generator);
}

template <typename T>
inline double dunif(const T value, const T lower, const T upper) {
  if (!between(value, lower, upper)) {
    return mininf;
  }
  return -log(upper - lower);
}

inline double dpois(const int k, const double lambda) {
  if (k < 0 || lambda < 0)
    return mininf;
  return k * log(lambda) - lambda - logfactorial(k);
}

template <typename URNG> size_t rpois(const double lambda, URNG &generator) {
  std::poisson_distribution<size_t> distribution(lambda);
  return distribution(generator);
}

inline double dgamma(const double value, const double shape,
                     const double scale) {
  if (value < 0 || shape < 0 || scale < 0)
    return mininf;
  return -loggamma(shape) - shape * log(scale) + (shape - 1) * log(value) -
         value / scale;
}

template <typename URNG> size_t rbinom(size_t n, double p, URNG &generator) {
  std::binomial_distribution<size_t> distribution(n, p);
  return distribution(generator);
}

// Returns log likelihood
inline double dbinom(size_t k, size_t n, double p) {
  if (k > n || p < 0 || p > 1)
    return mininf;
  return log_binom_coeff(n, k) + k * log(p) + (n - k) * log1p(-p);
}

inline double nbinom_convert_mu_to_p(double mu, double k) {
  return k / (k + mu);
};

// Negative binomial
// @param k is the number of successfull trials, does not strictly need to be an
// integer
// @param p probability of success
// @return the number of negative trials before k successfull ones
template <typename URNG> size_t rnbinom(double k, double p, URNG &generator) {
  std::negative_binomial_distribution<int> distribution(k, p);
  return distribution(generator);
}

// Negative binomial
// @param value is the number of negative trials, before k successfull trials
// @param k is the number of successfull trials, does not strictly need to be an
// integer
// @param p probability of success
inline double dnbinom(size_t value, double k, double p) {
  if (k < 0 || p < 0 || p > 1)
    return mininf;
  return log_binom_coeff(k + value - 1, (double)value) + k * log(p) +
         value * log1p(-p);
}

template <typename T, typename URNG>
Eigen::ArrayXi rmultinomial(size_t no_samples, const T &weights,
                            URNG &generator) {
  Eigen::ArrayXi samples(weights.size());
  auto norm = weights.sum();

  for (auto i = 0; i < weights.size(); i++) {
    if (weights[i] > 0.0 && no_samples > 0) {
      // TODO: Should rbinom check whether p is slightly bigger than 1? If so
      // then we can remove the next check
      //
      // Numerical issues can result in the last non zero weight to be bigger
      // than norm (p>1). We need to check for that, otherwise
      // std::binomial_distribution can throw
      if (weights[i] < norm) {
        samples[i] = rbinom(no_samples, weights[i] / norm, generator);
      } else {
        samples[i] = no_samples;
      }
    } else
      samples[i] = 0;

    no_samples -= samples[i];
    norm -= weights[i];
  }
  return samples;
}

// K = Eigen::ArrayXi, P = Eigen::ArrayXd?
template <typename K, typename P>
double dmultinomial(const K &k, size_t n, const P &p) {
  double lpdf = 0;
  double currP = 1;
  for (size_t i = 0; i < k.size() - 1; ++i) {
    lpdf += dbinom(k[i], n, p[i] / currP);
    currP -= p[i];
    n -= k[i];
  }
  return lpdf;
}

inline std::array<double, 2> beta_convert_mu_to_alpha_beta(double mu,
                                                           double sd) {
  std::array<double, 2> ab;
  auto v = pow(sd, 2);
  ab[0] = (-pow(mu, 3) + pow(mu, 2)) / v - mu;
  ab[1] = (pow(mu, 3) - 2 * pow(mu, 2) + mu) / v + mu - 1.0;
  return ab;
}

// This is the parameterization used by brms, which is simpler. phi is the
// precision parameter The reverse would be mu = alpha/(alph + beta), phi =
// alpha + beta
inline std::array<double, 2> beta_convert_mu_phi_to_alpha_beta(double mu,
                                                               double phi) {
  std::array<double, 2> ab;
  ab[0] = mu * phi;
  ab[1] = (1 - mu) * phi;
  return ab;
}

template <typename URNG>
double rbeta(double alpha, double beta, URNG &generator) {
  // Not sure if this is the fastest way, but we make use of (wikipedia):
  // Let X be a random number drawn from Gamma(α,1) and Y from Gamma(β,1), where
  // the first argument to the gamma distribution is the shape parameter. Then
  // Z=X/(X+Y) has distribution Beta(α,β)
  double x = std::gamma_distribution<double>(alpha, 1)(generator);
  double y = std::gamma_distribution<double>(beta, 1)(generator);
  return x / (x + y);
}

inline double dbeta(double x, double alpha, double beta) {
  if (x < 0 || x > 1 || alpha < 0 || beta < 0)
    return mininf;
  return loggamma(alpha + beta) - loggamma(alpha) - loggamma(beta) +
         (alpha - 1) * log(x) + (beta - 1) * log1p(-x);
}

template <typename URNG>
double rbetabinomial(int n, double alpha, double beta, URNG &generator) {
  // Draw from beta then binomial. Note that this might not be the most
  // efficient way of doing it, but it is the most straightforward
  double p = rbeta(alpha, beta, generator);
  return rbinom(n, p, generator);
}

inline double dbetabinomial(int k, int n, double alpha, double beta) {
  if (k < 0 || k > n || alpha < 0 || beta < 0)
    return mininf;
  return logfactorial(n) - (logfactorial(k) + logfactorial(n - k)) +
         loggamma(k + alpha) + loggamma(n - k + beta) -
         loggamma(n + alpha + beta) + loggamma(alpha + beta) -
         (loggamma(alpha) + loggamma(beta));
}

inline double dhypergeometric(int k, int N, int K, int n) {
  if (k < 0 || k > K || n < 0 || n > N || k > n || K > N)
    return mininf;
  return (log_binom_coeff(K, k) + log_binom_coeff(N - K, n - k)) -
         log_binom_coeff(N, n);
}

template <typename URNG>
size_t rhypergeometric(int N, int K, int n, URNG &generator) {
  size_t k = 0;
  for (size_t i = 0; i < n; ++i) {
    size_t dk = 0;

    if (K > 0 && N > 0)
      dk = rbinom(1, static_cast<double>(K) / N, generator);

    --N;
    K -= dk;
    k += dk;
  }
  return k;
}

template <typename URNG> double rnorm(double mu, double sd, URNG &generator) {
  std::normal_distribution<double> distribution(mu, sd);
  return distribution(generator);
}

inline double dnorm(const double x, const double mu, const double sd) {
  // x here is the actual value, while meanlog is the log transformed..
  if (sd < 0)
    return mininf;
  return -log(sd) - log(sqrt(2 * pi)) + (-pow((x - mu) / sd, 2) / 2);
}

template <class URNG>
Eigen::VectorXd rmvnorm(const Eigen::VectorXd &mu, const Eigen::MatrixXd &cov,
                        URNG &generator) {
  Eigen::VectorXd v(mu.size());
  for (auto i = 0; i < mu.size(); ++i)
    v(i) = rnorm(0.0, 1.0, generator);

  Eigen::MatrixXd transform;
  Eigen::LLT<Eigen::MatrixXd> cholSolver(cov);
  // We can only use the cholesky decomposition if
  // the covariance matrix is symmetric, pos-definite.
  // But a covariance matrix might be pos-semi-definite.
  // In that case, we'll go to an EigenSolver
  if (cholSolver.info() == Eigen::Success) {
    // Use cholesky solver
    transform = cholSolver.matrixL();
  } else {
    auto eigenSolver = Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd>(cov);
    transform = eigenSolver.eigenvectors() *
                eigenSolver.eigenvalues().cwiseMax(0).cwiseSqrt().asDiagonal();
  }

  return mu + transform * v;
}

inline double dmvnorm(const Eigen::VectorXd &x, const Eigen::VectorXd &mu,
                      const Eigen::MatrixXd &sigma) {

  double n = x.rows();
  double lsqrt2pi = 0.5 * log(2 * pi);
  double quadform = (x - mu).transpose() * sigma.inverse() * (x - mu);
  double lnorm = -n * lsqrt2pi - 0.5 * log(sigma.determinant());

  return lnorm + (-0.5 * quadform);
}

template <typename URNG>
double rlnorm(const double meanlog, const double sdlog, URNG &generator) {
  std::lognormal_distribution<double> distribution(meanlog, sdlog);
  return distribution(generator);
}

inline double dlnorm(const double x, const double meanlog, const double sdlog) {
  // x here is the actual value, while meanlog is the log transformed..
  if (x < 0 || sdlog < 0)
    return mininf;
  return -log(x) - log(sdlog) - log(sqrt(2 * pi)) +
         (-pow(log(x) - meanlog, 2) / (2 * pow(sdlog, 2)));
}

template <typename URNG>
double rexponential(const double lambda, URNG &generator) {
  std::exponential_distribution<double> distribution(lambda);
  return distribution(generator);
}

inline double dexponential(const double x, const double lambda) {
  if (x < 0 || lambda < 0)
    return mininf;
  return log(lambda) - x * lambda;
}

enum Pdf {
  Normal,
  LogNormal,
  NegativeBinomial,
  Uniform,
  Exponential,
  Beta,
  BetaBinomial,
  Binomial,
  Poisson,
  Hypergeometric
};

template <Pdf pdf, bool draw, typename D, typename URNG, typename... Args>
void unified_interface(double &logLikelihood, D &data, URNG &generator,
                       const Args &...args) {
  if constexpr (draw) {
    if constexpr (pdf == Normal) {
      data = rnorm(args..., generator);
    } else if constexpr (pdf == LogNormal) {
      data = rlnorm(args..., generator);
    } else if constexpr (pdf == NegativeBinomial) {
      data = rnbinom(args..., generator);
    } else if constexpr (pdf == Uniform) {
      data = runif(args..., generator);
    } else if constexpr (pdf == Exponential) {
      data = rexponential(args..., generator);
    } else if constexpr (pdf == Binomial) {
      data = rbinom(args..., generator);
    } else if constexpr (pdf == Poisson) {
      data = rpois(args..., generator);
    } else if constexpr (pdf == Beta) {
      data = rbeta(args..., generator);
    } else if constexpr (pdf == BetaBinomial) {
      data = rbetabinomial(args..., generator);
    } else if constexpr (pdf == Hypergeometric) {
      data = rhypergeometric(args..., generator);
    } else {
#ifdef USE_RCPP
      Rcpp::Rcerr << "Unknown distribution" << std::endl;
#else
      std::cerr << "Unknown distribution" << std::endl;
#endif
    }
  } else {
    if constexpr (pdf == Normal) {
      logLikelihood += dnorm(data, args...);
    } else if constexpr (pdf == LogNormal) {
      logLikelihood += dlnorm(data, args...);
    } else if constexpr (pdf == NegativeBinomial) {
      logLikelihood += dnbinom(data, args...);
    } else if constexpr (pdf == Uniform) {
      logLikelihood += dunif(data, args...);
    } else if constexpr (pdf == Exponential) {
      logLikelihood += dexponential(data, args...);
    } else if constexpr (pdf == Binomial) {
      logLikelihood += dbinom(data, args...);
    } else if constexpr (pdf == Beta) {
      logLikelihood += dbeta(data, args...);
    } else if constexpr (pdf == BetaBinomial) {
      logLikelihood += dbetabinomial(data, args...);
    } else if constexpr (pdf == Poisson) {
      logLikelihood += dpois(data, args...);
    } else if constexpr (pdf == Hypergeometric) {
      logLikelihood += dhypergeometric(data, args...);
    } else {
#ifdef USE_RCPP
      Rcpp::Rcerr << "Unknown distribution" << std::endl;
#else
      std::cerr << "Unknown distribution" << std::endl;
#endif
    }
  }
}
}; // namespace distribution
