#ifndef SMC_HPP
#define SMC_HPP

#ifndef FREESTANDING
#include <Rcpp.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wignored-attributes"
#include <RcppEigen.h>
#pragma GCC diagnostic pop
#else
#include <Eigen/Dense>
namespace Rcpp {
void warning(const std::string &str) { std::cout << str << std::endl; }
}; // namespace Rcpp
#endif

#include <chrono>
#include <functional>
#include <random>

#include "sample.hpp"

namespace smc {

/** Draw from a multinomial distribution
 *
 */
template <class URNG>
Eigen::ArrayXi rmultinomial(size_t no_samples, const Eigen::ArrayXd &weights,
                            URNG &generator) {
  Eigen::ArrayXi samples(weights.size());
  auto norm = weights.sum();

  for (auto i = 0; i < weights.size(); i++) {
    if (weights[i] > 0.0 && no_samples > 0) {
      // Numerical issues can result in the last non zero weight to be bigger
      // than norm (p>1). We need to check for that, otherwise
      // std::binomial_distribution can throw
      if (weights[i] < norm) {
        auto rbinom =
            std::binomial_distribution<int>(no_samples, weights[i] / norm);
        samples[i] = rbinom(generator);
      } else {
        samples[i] = no_samples;
      }
    } else
      samples[i] = 0;

    no_samples -= samples[i];
    norm -= weights[i];
  }
  return samples;
}

/** Rescale log weight values to result in reliable values

 Effectively rescales v in such a way that exp(v1)/exp(v2) is correct,
 including for very small values of v1 (i.e. values where exp(v1) is zero due to
 numerical errors)
*/
inline Eigen::ArrayXd relativeLogWeight(const Eigen::ArrayXd &v) {
  return (v - v.maxCoeff()).exp();
}

/** Calculate the log of the sum of values from the log of those values

 Calculates the result of log((exp(v1) + exp(v2) +... + exp(vn)))
 Importantly works for low values of v, where exp(vi) is effectively zero
 */
inline double log_sum_exponentials(const Eigen::ArrayXd &v) {
  auto mx = v.maxCoeff();
  return log((v - mx).exp().sum()) + mx;
}

// Check whether a class has a pack_history() member function
template <typename C, typename = void>
struct has_pack_history : std::false_type {};

template <typename C>
struct has_pack_history<
    C, typename std::enable_if<std::is_same<
           decltype(std::declval<C>().pack_history()), void>::value>::type>
    : std::true_type {};

/**
 * Particles container
 *
 * I have attempted smarter ones that kept a tree, but performance was much
 * lower than just reserving the needed space for the history. Note that this
 * does come at a cost of memory usage, because all Particles/States are stored
 * even if they are not resampled
 */
template <typename S> class Particles : public simplersmc::Samples<S> {
public:
  template <typename P>
  void initialize(size_t nparticles,
                  std::function<std::vector<S>(size_t n, const P &)> rinitN,
                  const P &params, size_t nsteps, bool history) {
    this->lweights.resize(nparticles);
    this->lweights.fill(0);
    this->samples = rinitN(nparticles, params);

    if (history) {
      this->store_id = 0;
      this->history_map.resize(nsteps);
      this->history_store.resize(nsteps);
      /*for (size_t i = 0; i < nsteps; ++i) {
        history_store[i].resize(nparticles);
        history_map[i].resize(nparticles);
      }*/
    }
  }

  void step(bool resample = true, bool store = false) {
    if (!store) {
      simplersmc::Samples<S>::step(resample);
    } else {
      // TODO: can we unify this with the step function
      // seems a bit of a maintenance burden to have to maintain both
      // separately
      size_t n = this->samples.size();
      if (resample)
        n = this->ids.size();
      this->copySamples.resize(n);
      for (size_t i = 0; i < n; ++i) {
        auto j = i;
        if (resample)
          j = this->ids[i];
        this->copySamples[i] = this->samples[j];
      }
      if constexpr (has_pack_history<S>::value) {
        for (auto &&s : this->samples)
          s.pack_history();
      }
      history_store[this->store_id] = std::move(this->samples);
      history_map[this->store_id] = std::move(this->ids);

      // Wasn't sure if after a move this->samples/this->ids is still a
      // valid vector or totally destroyed.
      // Specs says: After the move, other is guaranteed to be empty()
      // So should still be valid
      this->samples = std::move(this->copySamples);
      ++this->store_id;
    }
  }

  std::vector<S> get_particle_history(size_t j) {
    std::vector<S> history(this->history_store.size());
    for (int i = (history_store.size() - 1); i >= 0; --i) {
      if (history_map[i].size() > j)
        j = history_map[i][j];
      history[i] = history_store[i][j];
    }
    return history;
  }

  size_t nhistory() {
    if (this->history_store.size() == 0)
      return 0;
    return this->history_store.back().size();
  }

private:
  size_t store_id = 0;
  std::vector<std::vector<S>> history_store;
  std::vector<std::vector<size_t>> history_map;
};

/// Resampling using the systematic algorithm
template <typename S, class URNG>
S resampling_systematic(S &&particles, size_t nparticles, URNG &generator) {
  static std::uniform_real_distribution<double> runif(0.0, 1.0);
  auto u = runif(generator);

  // TODO: doesn't the way we currently calculate relativeLogWeight
  // ensure that weights.sum() == 1?
  auto weights = relativeLogWeight(particles.lweights);
  auto w = weights / weights.sum();
  auto cdf = w[0];
  auto j = 0;
  for (size_t i = 0; i < nparticles; ++i) {
    auto v = (i + u) / nparticles;
    while (v > cdf) {
      ++j;
      cdf += w[j];
    }
    particles.set(i, j);
  }
  // After resampling all weights are equal again
  particles.lweights.resize(nparticles);
  particles.lweights.fill(0);

  return std::move(particles);
}

/// Resampling using the multinom algorithm
template <typename S, class URNG>
S resampling_multinom(S &&particles, size_t nparticles, URNG &generator) {
  auto weights = relativeLogWeight(particles.lweights);
  auto samples = rmultinomial(nparticles, weights, generator);
  size_t k = 0;
  for (size_t i = 0; i < samples.size(); ++i) {
    for (auto j = 0; j < samples[i]; ++j) {
      particles.set(k, i);
      ++k;
    }
  }
  // After resampling all weights are equal again
  particles.lweights.resize(nparticles);
  particles.lweights.fill(0);

  return std::move(particles);
}

// The force methods, resample every step. This is mainly useful for testing
enum Resampling {
  systematic,
  force_systematic,
  multinom,
  force_multinom,
  none
};

inline auto is_resampling_needed(Resampling method,
                                 const Eigen::ArrayXd &lweights,
                                 size_t nparticles) {
  bool resampling = false;
  auto log_weight_sum = log_sum_exponentials(lweights);
  if (method == force_multinom || method == force_systematic) {
    resampling = true;
  } else if (method != none && nparticles > 1) {
    auto log_weight_sum_squared = log_sum_exponentials(lweights * 2);
    // Calculate Kish's effective sample size
    auto neff = exp(log_weight_sum * 2 - log_weight_sum_squared);
    if (neff < nparticles / 2.0) {
      resampling = true;
    }
  }

  return std::make_tuple(resampling, log_weight_sum);
}

template <typename S, typename D, typename P> class Model {
public:
  Resampling resampling_method = systematic;
  size_t no_times_resampled = 0;

  Model() {
    rinitN = [this](size_t n, const P &params) {
      std::vector<S> states(n);
      for (size_t i = 0; i < n; ++i) {
        states[i] = this->rinit(params);
      }
      return states;
    };

    rprocessN = [this](size_t n, std::vector<S> &&states, const P &params) {
      for (size_t i = 0; i < n; ++i) {
        states[i] = this->rprocess(std::move(states[i]), params);
      }
      return std::move(states);
    };

    rmeasureN = [this](size_t n, const std::vector<S> &states,
                       const P &params) {
      if (this->rmeasure) {
        std::vector<D> data(n);
        if (n > states.size())
          Rcpp::warning("Incompatible lengths detected in rmeasureN");
        for (size_t i = 0; i < std::min(n, states.size()); ++i) {
          data[i] = this->rmeasure(states[i], params);
        }
        return data;
      }
      // Return empty
      return std::vector<D>();
    };

    dmeasureN = [this](size_t n, const D &data, const std::vector<S> &states,
                       const P &params) {
      Eigen::ArrayXd lweights(n);
      for (size_t i = 0; i < n; ++i) {
        lweights[i] = this->dmeasure(data, states[i], params);
      }
      return lweights;
    };

    auto seed = std::chrono::duration_cast<std::chrono::milliseconds>(
                    std::chrono::system_clock::now().time_since_epoch())
                    .count();
    this->generator.seed(seed);
  }

  ~Model() {}

  double logLikelihood = 0;
  Particles<S> particles;

  std::function<S(const P &)> rinit;
  std::function<std::vector<S>(size_t n, const P &)> rinitN;

  std::function<S(S &&, const P &)> rprocess;
  std::function<std::vector<S>(size_t n, std::vector<S> &&, const P &)>
      rprocessN;

  std::function<D(const S &, const P &)> rmeasure;
  std::function<std::vector<D>(size_t n, const std::vector<S> &, const P &)>
      rmeasureN;

  std::function<double(const D &, const S &, const P &)> dmeasure;
  std::function<Eigen::ArrayXd(size_t n, const D &, const std::vector<S> &,
                               const P &)>
      dmeasureN;

  void simulate(size_t nsim, const P &params, const std::vector<int> &times,
                int t0) {
    if (times.size() == 0)
      return;
    this->particles.initialize(nsim, this->rinitN, params, times.size(), true);

    for (auto i = 0; i < times.size(); ++i) {
      for (auto j = 0; j < times[i] - t0; ++j) {
        this->particles.samples = std::move(
            rprocessN(nsim, std::move(this->particles.samples), params));
      }
      // Note that the first step should not be stored
      // Probably should have store = false for first step

      this->particles.step(false, true);

      t0 = times[i];
    }
  }

  double smc(size_t nparticles, const P &params, const std::vector<D> &data,
             const std::vector<int> &times, int t0, bool history = false) {
    // TODO: refactor the shared code with simulate
    logLikelihood = 0;
    if (times.size() == 0)
      return logLikelihood;

    this->particles.initialize(nparticles, this->rinitN, params, times.size(),
                               history);

    double log_weight_sum = 0;
    bool resample = false;

    for (auto i = 0; i < times.size(); ++i) {
      for (auto j = 0; j < times[i] - t0; ++j) {
        this->particles.samples = std::move(
            rprocessN(nparticles, std::move(this->particles.samples), params));
      }

      // Calculate lweights
      this->particles.lweights +=
          this->dmeasureN(nparticles, data[i], this->particles.samples, params);
      // Convert nan's into -infinite. This is mainly to deal with
      // R's oddities when parameters are outside of the acceptable range
      this->particles.lweights = this->particles.lweights.unaryExpr(
          [](double v) { return std::isnan(v) ? log(0) : v; });

      std::tie(resample, log_weight_sum) = is_resampling_needed(
          resampling_method, this->particles.lweights, nparticles);

      if (!std::isfinite(log_weight_sum) || !std::isfinite(logLikelihood)) {
        logLikelihood = log(0);
        return logLikelihood;
      } else if (resample) {
        ++no_times_resampled;
        // log_weight_sum - log(nparticles) is the mean weight
        logLikelihood += log_weight_sum - log(nparticles);

        switch (resampling_method) {
        case systematic:
        case force_systematic:
          this->particles.ids.resize(nparticles);
          this->particles = resampling_systematic<Particles<S>>(
              std::move(this->particles), nparticles, this->generator);
          break;
        case multinom:
        case force_multinom:
          this->particles.ids.resize(nparticles);
          this->particles = resampling_multinom<Particles<S>>(
              std::move(this->particles), nparticles, this->generator);
          break;
        default:
#ifdef FREESTANDING
          std::cerr << "Resampling error unknown resampling method"
                    << std::endl;
#else
          Rcpp::Rcerr << "Resampling error unknown resampling method"
                      << std::endl;
#endif
          resample = false;
        }
      }

      this->particles.step(resample, history);

      t0 = times[i];
    }

    // We did not resample last step, so calculate ll now
    if (!resample) {
      // log_weight_sum - log(nparticles) is the mean weight
      logLikelihood += log_weight_sum - log(nparticles);
    }

    return logLikelihood;
  }

  size_t adapt_particles(size_t min_nparticles, size_t max_nparticles,
                         const P &params, const std::vector<D> &data,
                         const std::vector<int> &times, int t0) {
    size_t nparticles = min_nparticles;
    while (nparticles < max_nparticles) {
      size_t count = 0;
      double mean = 0;
      double m2 = 0;
      for (size_t i = 0; i < 250; ++i) {
        this->smc(nparticles, params, data, times, t0, false);
        if (std::isfinite(this->logLikelihood)) {
          ++count;
          auto delta = this->logLikelihood - mean;
          mean += delta / count;
          auto delta2 = this->logLikelihood - mean;
          m2 += delta * delta2;
        } else {
          m2 = -log(0);
          break;
        }
      }
      double var = m2 / count;
#ifdef FREESTANDING
      std::cout << nparticles << " particles, logLikelihood variance: " << var
                << std::endl;
#else
      Rcpp::Rcout << nparticles << " particles, logLikelihood variance: " << var
                  << std::endl;
#endif
      if (var < 1.0)
        break;
      nparticles *= 2;
      if (nparticles >= max_nparticles) {
        if (!std::isfinite(m2)) {
          Rcpp::warning(
              "Some samples returned infinite log likelihood. You probably "
              "need to adjust your starting parameters, so that the log "
              "likelihood is not -Inf. Or increase the number of particles "
              "beyond the specified max value.");
        } else
          Rcpp::warning(
              "Final variance higher than 1.0. It is recommended to increase "
              "the max number of particles.");
      }
    }
    return std::min(nparticles, max_nparticles);
  }

  std::vector<std::vector<S>> get_particles(size_t nparticles = 0) {
    if (nparticles == 0 || nparticles > particles.nhistory())
      nparticles = particles.nhistory();

    std::vector<std::vector<S>> histories(nparticles);

    // need to resample to ensure equal weights
    simplersmc::Samples<S> s;
    s.lweights = particles.lweights;
    s.ids.resize(particles.lweights.size());
    std::iota(s.ids.begin(), s.ids.end(), 0);
    // Always use systematic here. Note that it ensures no change if weights
    // were already equal
    s = resampling_systematic<simplersmc::Samples<S>>(
        std::move(s), particles.lweights.size(), this->generator);

    // Shuffle the outcome
    std::vector<size_t> ids(particles.lweights.size());
    std::iota(ids.begin(), ids.end(), 0);
    std::shuffle(ids.begin(), ids.end(), this->generator);

    for (size_t i = 0; i < nparticles; ++i)
      histories[i] = particles.get_particle_history(s.ids[ids[i]]);
    return histories;
  }

  std::vector<std::vector<D>>
  get_measurements(const std::vector<std::vector<S>> &particles,
                   const P &params) const {
    std::vector<std::vector<D>> smpls(particles.size());
    for (size_t i = 0; i < particles.size(); ++i) {
      smpls[i] = rmeasureN(particles[i].size(), particles[i], params);
    }
    return smpls;
  }

  std::vector<std::vector<D>> get_measurements(size_t nparticles,
                                               const P &params) {
    return this->get_measurements(get_particles(nparticles), params);
  }

  void set_seed(size_t seed) { generator.seed(seed); }

private:
  std::default_random_engine generator;
};

} // namespace smc
#endif
