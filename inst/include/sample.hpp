#ifndef SAMPLE_HPP
#define SAMPLE_HPP

#ifndef FREESTANDING
#include <Rcpp.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wignored-attributes"
#include <RcppEigen.h>
#pragma GCC diagnostic pop
#else
#include <Eigen/Dense>
#endif

#include <vector>

namespace simplersmc {

/**
 * This is an internal class and has to be used with caution.
 * In general you can pass this to any resampler, which will call
 * the set function. Afterwards you call step, which will copy
 * the selected samples. You need to pass a new size to the
 * step, but be careful that this size should be the same size as
 * passed to the resampler (for most resamplers).
 *
 * For the none resampler you will want to set the ids initially with std::iota
 *
 * Currently resizing is done by resizing ids.size() before resampling. The
 * resampler will grow/shring lweights as needed, and step() will create the
 * newly sized samples
 */
template <typename T> class Samples {
public:
  std::vector<T> samples;
  std::vector<T> copySamples;
  Eigen::ArrayXd lweights;
  std::vector<size_t> ids;

  Samples() {}

  size_t size() { return ids.size(); }

  // Needed for compatibility with resampling algorithms
  void set(size_t i, size_t j) { this->ids[i] = j; }

  void step(bool resample = true) {
    if (!resample)
      return;

    copySamples.resize(ids.size());
    size_t prev_j = 0;
    for (size_t i = 0; i < ids.size(); ++i) {
      auto j = this->ids[i];
      /*
       Reduce number of copies, by moving the first new sample
       Otherwise copy the just moved sample
       Relies on j (this->ids[i]) to be in ascending order, which it
       should be with our current resampling methods
      */
      if (j > prev_j || i == 0) {
        prev_j = j;
        copySamples[i] = std::move(this->samples[j]);
      } else if (j == prev_j) {
        copySamples[i] = copySamples[i - 1];
      } else {
        /*
           If we ever implement a resamping method where j is not ordered we could
           fallback for that resampler to the old code
           copySamples[i] = this->samples[j]
           Note that we can't just do it in this else case, because the move
           above will have invalidated this->samples[j]
        */
        std::string msg = "[SAMPLE_HPP] Sampling error";
#ifdef FREESTANDING
        std::cerr << msg << std::endl;
#else
        Rcpp::stop(msg);
#endif
      }
    }
    this->samples = std::move(copySamples);
  }

private:
};
}; // namespace simplersmc

#endif
