#include <Rcpp.h>

#include "../inst/include/sample.hpp"
#include "../inst/include/smc.hpp"

using Samples = simplersmc::Samples<Rcpp::RObject>;

//' @title Resample a list of objects according to the given log weights using multinomial resampling
//'
//' @param n Number of samples to return
//' @param samples List with the objects to sample from
//' @param lweights Log weights corresponding to each sample
//'
// [[Rcpp::export]]
std::vector<Rcpp::RObject> resampling_multinom(size_t n, const std::vector<Rcpp::RObject> &samples, 
    const Eigen::ArrayXd &lweights) {
  std::default_random_engine generator;
  Samples s;
  s.samples = samples;
  s.lweights = lweights;
  s.ids.resize(n);
  s = smc::resampling_multinom(std::move(s), n, generator);
  s.step();
  return(s.samples);
}


//' @title Resample a list of objects according to the given log weights using systematic resampling
//'
//' @param n Number of samples to return
//' @param samples List with the objects to sample from
//' @param lweights Log weights corresponding to each sample
//'
// [[Rcpp::export]]
std::vector<Rcpp::RObject> resampling_systematic(size_t n, const std::vector<Rcpp::RObject> &samples, 
    const Eigen::ArrayXd &lweights) {
  std::default_random_engine generator;
  Samples s;
  s.samples = samples;
  s.lweights = lweights;
  s.ids.resize(n);
  s = smc::resampling_systematic(std::move(s), n, generator);
  s.step();
  return(s.samples);
}
