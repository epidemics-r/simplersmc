#include <Rcpp.h>

#include "../inst/include/npmc.hpp"

using RcppNPMCModel = npmc::Model<Rcpp::RObject>;

void rprior(RcppNPMCModel* model, Rcpp::Function rprior) {
  auto func = [rprior]() {
    PutRNGstate();
    auto lw = rprior();
    GetRNGstate();
    return Rcpp::as<Eigen::VectorXd>(lw);
  };
  model->rprior = func;
}

void rpriorN(RcppNPMCModel* model, Rcpp::Function rprior) {
  auto func = [rprior](size_t n) {
    PutRNGstate();
    auto lw = rprior(n);
    GetRNGstate();
    return Rcpp::as<std::vector<Eigen::VectorXd>>(lw);
  };
  model->rpriorN = func;
}

void dprior(RcppNPMCModel* model, Rcpp::Function dprior) {
  auto func = [dprior](Eigen::VectorXd params) {
    PutRNGstate();
    auto lw = dprior(params);
    GetRNGstate();
    return Rcpp::as<double>(lw);
  };
  model->dprior = func;
}

void dpriorN(RcppNPMCModel* model, Rcpp::Function dprior) {
  auto func = [dprior](size_t n, std::vector<Eigen::VectorXd> params) {
    PutRNGstate();
    auto lws = dprior(n, params);
    GetRNGstate();
    return Rcpp::as<Eigen::ArrayXd>(lws);
  };
  model->dpriorN = func;
}

void dmeasure(RcppNPMCModel* model, Rcpp::Function dmeasure) {
  auto func = [dmeasure](Rcpp::RObject data, Eigen::VectorXd params) {
    PutRNGstate();
    auto lw = dmeasure(data, params);
    GetRNGstate();
    return Rcpp::as<double>(lw);
  };
  model->dmeasure = func;
}

void dmeasureN(RcppNPMCModel* model, Rcpp::Function dmeasure) {
  auto func = [dmeasure](size_t n, Rcpp::RObject data,
                         std::vector<Eigen::VectorXd> params) {
    PutRNGstate();
    auto lws = dmeasure(n, data, params);
    GetRNGstate();
    return Rcpp::as<Eigen::ArrayXd>(lws);
  };
  model->dmeasureN = func;
}

Rcpp::List step(RcppNPMCModel* model, size_t nsamples, Rcpp::RObject data,
                bool first) {
  auto smpls = model->step(nsamples, data, 0, first);
  Rcpp::List lst(0);
  // lst["samples"] = Rcpp::as<Eigen::ArrayXd>(smpls.samples);
  lst["samples"] = smpls.samples;
  lst["lweights"] = smpls.lweights;
  return lst;
}

Rcpp::List inference(RcppNPMCModel* model, size_t niterations, size_t nsamples,
                     Rcpp::RObject data) {
  auto smpls = model->inference(niterations, nsamples, data);
  Rcpp::List lst(nsamples);
  for (auto i = 0; i < nsamples; ++i) lst[i] = smpls[i];
  return lst;
}

RCPP_MODULE(RcppNPMCModule) {
  using namespace Rcpp;

  class_<RcppNPMCModel>("NPMCModel")
      .default_constructor()
      .method("rprior", &rprior)
      .method("rpriorN", &rpriorN)
      .method("dprior", &dprior)
      .method("dpriorN", &dpriorN)
      .method("dmeasure", &dmeasure)
      .method("dmeasureN", &dmeasureN)
      .method("step", &step)
      .method("inference", &inference);
}
