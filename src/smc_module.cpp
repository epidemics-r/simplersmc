#include <Rcpp.h>

#include "../inst/include/smc.hpp"

using RcppSMCModel = smc::Model<Rcpp::RObject, Rcpp::RObject, Rcpp::RObject>;

void rinit(RcppSMCModel *model, Rcpp::Function rinit) {
  auto func = [rinit](const Rcpp::RObject &params) {
    PutRNGstate();
    auto newState = rinit(params);
    GetRNGstate();
    return newState;
  };
  model->rinit = func;
}

void rinitN(RcppSMCModel *model, Rcpp::Function rinit) {
  auto func = [rinit](size_t n, const Rcpp::RObject &params) {
    PutRNGstate();
    auto newStates = rinit(n, params);
    GetRNGstate();
    return Rcpp::as<std::vector<Rcpp::RObject>>(newStates);
  };
  model->rinitN = func;
}

void rprocess(RcppSMCModel *model, Rcpp::Function rprocess) {
  auto func = [rprocess](Rcpp::RObject &&state, const Rcpp::RObject &params) {
    PutRNGstate();
    auto newState = rprocess(state, params);
    GetRNGstate();
    return newState;
  };
  model->rprocess = func;
}

void rprocessN(RcppSMCModel *model, Rcpp::Function rprocess) {
  auto func = [rprocess](size_t n, std::vector<Rcpp::RObject> &&states,
                         const Rcpp::RObject &params) {
    PutRNGstate();
    auto newStates = rprocess(n, states, params);
    GetRNGstate();
    return Rcpp::as<std::vector<Rcpp::RObject>>(newStates);
  };
  model->rprocessN = func;
}

void rmeasure(RcppSMCModel *model, Rcpp::Function rmeasure) {
  auto func = [rmeasure](const Rcpp::RObject &state,
                         const Rcpp::RObject &params) {
    PutRNGstate();
    auto rData = rmeasure(state, params);
    GetRNGstate();
    return rData;
  };
  model->rmeasure = func;
}

void rmeasureN(RcppSMCModel *model, Rcpp::Function rmeasure) {
  auto func = [rmeasure](size_t n, const std::vector<Rcpp::RObject> &state,
                         const Rcpp::RObject &params) {
    PutRNGstate();
    auto rData = rmeasure(n, state, params);
    GetRNGstate();
    return Rcpp::as<std::vector<Rcpp::RObject>>(rData);
  };
  model->rmeasureN = func;
}

void dmeasure(RcppSMCModel *model, Rcpp::Function dmeasure) {
  auto func = [dmeasure](const Rcpp::RObject &data, const Rcpp::RObject &state,
                         const Rcpp::RObject &params) {
    PutRNGstate();
    auto lw = dmeasure(data, state, params);
    GetRNGstate();
    return Rcpp::as<double>(lw);
  };
  model->dmeasure = func;
}

void dmeasureN(RcppSMCModel *model, Rcpp::Function dmeasure) {
  auto func = [dmeasure](size_t n, const Rcpp::RObject &data,
                         const std::vector<Rcpp::RObject> &states,
                         const Rcpp::RObject &params) {
    PutRNGstate();
    auto lws = dmeasure(n, data, states, params);
    GetRNGstate();
    return Rcpp::as<Eigen::ArrayXd>(lws);
  };
  model->dmeasureN = func;
}

void simulate(RcppSMCModel *model, size_t nsim, const Rcpp::RObject &params,
              const std::vector<int> &times, size_t t0) {
  model->simulate(nsim, params, times, t0);
}

void set_resampling_method(RcppSMCModel *model, std::string_view resampling_method) {
  model->resampling_method = smc::systematic;
  if (resampling_method == "multinom")
    model->resampling_method = smc::multinom;
  else if (resampling_method == "none")
    model->resampling_method = smc::none;
  else if (resampling_method == "force_systematic")
    model->resampling_method = smc::force_systematic;
  else if (resampling_method == "force_multinom")
    model->resampling_method = smc::force_multinom;
}

void runsmc(RcppSMCModel *model, size_t nparticles, const Rcpp::RObject &params,
            const std::vector<Rcpp::RObject> &data,
            const std::vector<int> &times, int t0, bool history,
            const std::string &resampling_method) {
  set_resampling_method(model, resampling_method);
  model->smc(nparticles, params, data, times, t0, history);
}

size_t adapt_particles(RcppSMCModel *model, size_t min_nparticles,
                       size_t max_nparticles, const Rcpp::RObject &params,
                       const std::vector<Rcpp::RObject> &data,
                       const std::vector<int> &times, int t0,
                       const std::string &resampling_method) {
  set_resampling_method(model, resampling_method);

  return model->adapt_particles(min_nparticles, max_nparticles, params, data,
                                times, t0);
}

Rcpp::List particles(RcppSMCModel *model) {
  // TODO: Currently gets raw particles, might be risky to have accessible
  // But it does mean that every time we call it we get consistent results
  // back, in contrast to get_particles, who mixes the results around.
  auto n = model->particles.nhistory();
  Rcpp::List lst(n);
  for (size_t i = 0; i < n; ++i) {
    auto hist = model->particles.get_particle_history(i);
    lst[i] = hist;
  }
  return lst;
}

Rcpp::List get_particles(RcppSMCModel *model, size_t n) {
  auto particles = model->get_particles(n);
  n = particles.size();
  Rcpp::List lst(n);
  for (size_t i = 0; i < n; ++i) {
    auto hist = particles[i];
    lst[i] = hist;
  }
  return lst;
}

Rcpp::List get_measurements(RcppSMCModel *model, size_t n,
                            const Rcpp::RObject &params) {
  auto samples = model->get_measurements(n, params);
  n = samples.size();
  Rcpp::List lst(n);
  for (size_t i = 0; i < n; ++i) {
    auto hist = samples[i];
    lst[i] = hist;
  }
  return lst;
}

RCPP_MODULE(RcppSMCModule) {
  using namespace Rcpp;

  class_<RcppSMCModel>("SMCModel")
      .default_constructor()
      .method("rinit", &rinit)
      .method("rinitN", &rinitN)
      .method("rprocess", &rprocess)
      .method("rprocessN", &rprocessN)
      .method("rmeasure", &rmeasure)
      .method("rmeasureN", &rmeasureN)
      .method("dmeasure", &dmeasure)
      .method("dmeasureN", &dmeasureN)
      .method("simulate", &simulate)
      .method("smc", &runsmc)
      .method("adapt_particles", &adapt_particles)
      .method("get_particles", &get_particles)
      .method("get_measurements", &get_measurements)
      .property("particles", &particles,
                "Modelled particles. If history was not tracked, then all "
                "histories will be empty.")
      .field_readonly("no_times_resampled", &RcppSMCModel::no_times_resampled)
      .field_readonly("logLikelihood", &RcppSMCModel::logLikelihood);
}
