// [[Rcpp::depends(RcppEigen)]]

#include <smc.hpp>

// [[Rcpp::export]]
Eigen::ArrayXi rmultinom_r(size_t nparticles, const Eigen::ArrayXd weights, int seed = 1) {
  std::default_random_engine generator(seed);
  return smc::rmultinomial(nparticles, weights, generator);
}

