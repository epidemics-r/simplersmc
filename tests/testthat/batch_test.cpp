// [[Rcpp::depends(RcppEigen)]]
// [[Rcpp::depends(RcppParallel)]]

#include <smc.hpp>
#include <parallel.hpp>

#include <atomic>
#include <numeric>

constexpr double pi = 3.14159265358979323846;

template <typename URNG> double rnorm(double mu, double sd, URNG &generator) {
  std::normal_distribution<double> distribution(mu, sd);
  return distribution(generator);
}

double dnorm(const double x, const double mu, const double sd) {
  // x here is the actual value, while meanlog is the log transformed..
  if (sd < 0)
    return -std::numeric_limits<double>::infinity();
  return -log(sd) - log(sqrt(2 * pi)) + (-pow((x - mu) / sd, 2) / 2);
}

template <typename URNG> size_t rbinom(size_t n, double p, URNG &generator) {
  std::binomial_distribution<size_t> distribution(n, p);
  return distribution(generator);
}

template <typename T> inline double log_binom_coeff(T n, T k) {
  return lgamma(n + 1) - (lgamma(k + 1) + lgamma(n - k + 1));
}

// Returns log likelihood
inline double dbinom(size_t k, size_t n, double p) {
  if (k > n || p < 0 || p > 1)
    return -std::numeric_limits<double>::infinity();
  return log_binom_coeff(n, k) + k * log(p) + (n - k) * log(1 - p);
}

template <typename URNG>
double runif(const double mn, const double mx, URNG &generator) {
  std::uniform_real_distribution<double> distribution(mn, mx);
  return distribution(generator);
}

struct State {
  double h;
  double x;
  size_t time = 0;

  int copy_count = 0;
  int move_count = 0;

  State() {}

  State(const State &obj)
      : h(obj.h), x(obj.x), copy_count(obj.copy_count + 1),
        move_count(obj.move_count), time(obj.time) {}

  State &operator=(const State &obj) {
    this->h = obj.h;
    this->x = obj.x;
    this->copy_count = obj.copy_count + 1;
    this->move_count = obj.move_count;
    this->time = obj.time;
    return *this;
  }

  State(const State &&obj)
      : h(std::move(obj.h)), x(std::move(obj.x)),
        copy_count(std::move(obj.copy_count)),
        move_count(std::move(obj.move_count) + 1), time(std::move(obj.time)) {}

  State &operator=(const State &&obj) {
    this->h = std::move(obj.h);
    this->x = std::move(obj.x);
    this->copy_count = std::move(obj.copy_count);
    this->move_count = std::move(obj.move_count) + 1;
    this->time = std::move(obj.time);
    return *this;
  }

  ~State() {}
};

struct Data {
  // -1 indicates that no data was available for that time step
  int y_h = -1;
  double y_x = -1;
};

namespace Rcpp {
template <> SEXP wrap(const State &data) {
  Rcpp::List RData(0);
  RData["h"] = data.h;
  RData["x"] = data.x;
  RData["copy_count"] = data.copy_count;
  RData["move_count"] = data.move_count;
  RData["time"] = data.time;
  return Rcpp::wrap(RData);
}

template <> SEXP wrap(const Data &data) {
  Rcpp::List RData(0);
  RData["y_h"] = data.y_h;
  RData["y_x"] = data.y_x;
  return Rcpp::wrap(RData);
}

template <> Data as(SEXP dataRObj) {
  Data data;
  auto dR = Rcpp::as<Rcpp::List>(dataRObj);
  data.y_h = dR["y_h"];
  data.y_x = dR["y_x"];
  return data;
}
}; // namespace Rcpp

State rprocess(State &&state, const Eigen::ArrayXd &params, double hrate) {
  state.h = state.h * hrate;
  state.x = params[2] * state.h;
  ++state.time;
  return std::move(state);
};

// Input is dummy variable (for now), but used to make sure we can pass input to
// the batchworker
smc::Model<State, Data, Eigen::ArrayXd>
build_model(const std::vector<int> &input, std::default_random_engine &generator) {
  smc::Model<State, Data, Eigen::ArrayXd> model;

  model.resampling_method = smc::multinom;

  model.rinit = [](const Eigen::ArrayXd &params) {
    State state;
    state.h = params[0];
    return state;
  };

  // Using the looped version here. Note that the looped version is called
  // rprocessN
  model.rprocessN = [&generator](size_t n, std::vector<State> &&states,
                       const Eigen::ArrayXd &params) {
    for (size_t i = 0; i < n; ++i)
      states[i] = rprocess(std::move(states[i]), params, rnorm(1.0, params[1], generator));
    return std::move(states);
  };

  model.dmeasure = [](const Data &data, const State &state,
                      const Eigen::ArrayXd &params) {
    double ll = 0;
    if (state.x < 0 || state.h < 0 || state.h > 1)
      return -std::numeric_limits<double>::infinity();
    ll = dnorm(data.y_x, state.x, 0.2);
    if (data.y_h >= 0) {
      ll = ll + dbinom(data.y_h, 5, state.h);
    }
    return ll;
  };

  model.rmeasure = [&generator](const State &state, const Eigen::ArrayXd &params) {
    Data data;
    data.y_x = rnorm(state.x, 0.2, generator);
    auto rnd = runif(0, 1, generator);
    if (rnd < 0.2) {
      data.y_h = rbinom(5, state.h, generator);
    }
    return data;
  };

  return model;
}

// [[Rcpp::export]]
std::vector<Data> simulate_cpp(size_t nparticles, Eigen::ArrayXd params,
                               std::vector<int> times) {
  std::default_random_engine generator;
  auto model = build_model({}, generator);
  model.simulate(nparticles, params, times, 0);
  auto res = model.get_measurements(nparticles, params);
  return res[0];
}

// [[Rcpp::export]]
std::vector<double> batch_smc(size_t nparticles, const Eigen::MatrixXd &params,
                              const std::vector<Data> &data,
                              const std::vector<int> &input) {
  // Function should accept a generator, because that needs to be passed
  // from batchworker
  auto build_f = [&input](std::default_random_engine &generator) {
    return build_model(input, generator);
  };
  std::vector<int> times(data.size());
  std::iota(times.begin(), times.end(), 1);
  return smc::batch_smc<State>(nparticles, params, data, times, 0, build_f);
}

// [[Rcpp::export]]
std::vector<Rcpp::List> batch_smc_history(size_t nparticles, const Eigen::MatrixXd &params,
                             const std::vector<Data> &data,
                             const std::vector<int> &input) {
  // Function should accept a generator, because that needs to be passed
  // from batchworker
  auto build_f = [&input](std::default_random_engine &generator) {
    return build_model(input, generator);
  };
  std::vector<int> times(data.size());
  std::iota(times.begin(), times.end(), 1);
  return smc::batch_smc_history<State>(nparticles, params, data, times, 0, build_f);
}

// [[Rcpp::export]]
std::vector<Rcpp::List> batch_smc_history_no_rmeasure(size_t nparticles, const Eigen::MatrixXd &params,
                             const std::vector<Data> &data,
                             const std::vector<int> &input) {
  // Function should accept a generator, because that needs to be passed
  // from batchworker
  auto build_f = [&input](std::default_random_engine &generator) {
    auto mdl = build_model(input, generator);
    mdl.rmeasure = NULL;
    return mdl;
  };
  std::vector<int> times(data.size());
  std::iota(times.begin(), times.end(), 1);
  return smc::batch_smc_history<State>(nparticles, params, data, times, 0, build_f);
}
