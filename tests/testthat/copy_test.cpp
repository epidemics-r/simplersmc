// [[Rcpp::depends(RcppEigen)]]

#include <smc.hpp>

#include <atomic>

struct State {
  double h;
  double x;
  size_t time = 0;

  int copy_count = 0;
  int move_count = 0;

  State() {}

  State(const State &obj)
      : h(obj.h), x(obj.x), copy_count(obj.copy_count + 1),
        move_count(obj.move_count), time(obj.time) {}

  State &operator=(const State &obj) {
    this->h = obj.h;
    this->x = obj.x;
    this->copy_count = obj.copy_count + 1;
    this->move_count = obj.move_count;
    this->time = obj.time;
    return *this;
  }

  State(const State &&obj)
      : h(std::move(obj.h)), x(std::move(obj.x)),
        copy_count(std::move(obj.copy_count)),
        move_count(std::move(obj.move_count) + 1), time(std::move(obj.time)) {}

  State &operator=(const State &&obj) {
    this->h = std::move(obj.h);
    this->x = std::move(obj.x);
    this->copy_count = std::move(obj.copy_count);
    this->move_count = std::move(obj.move_count) + 1;
    this->time = std::move(obj.time);
    return *this;
  }

  ~State() {}
};

struct Data {
  // -1 indicates that no data was available for that time step
  int y_h = -1;
  int y_x = -1;
};

namespace Rcpp {
template <> SEXP wrap(const State &data) {
  Rcpp::List RData(0);
  RData["h"] = data.h;
  RData["x"] = data.x;
  RData["copy_count"] = data.copy_count;
  RData["move_count"] = data.move_count;
  RData["time"] = data.time;
  return Rcpp::wrap(RData);
}

template <> SEXP wrap(const Data &data) {
  Rcpp::List RData(0);
  RData["y_h"] = data.y_h;
  RData["y_x"] = data.y_x;
  return Rcpp::wrap(RData);
}

template <> Data as(SEXP dataRObj) {
  Data data;
  auto dR = Rcpp::as<Rcpp::List>(dataRObj);
  data.y_h = dR["y_h"];
  data.y_x = dR["y_x"];
  return data;
}
}; // namespace Rcpp

State rprocess(State &&state, const std::vector<double> &params, double hrate) {
  state.h = state.h * hrate;
  state.x = params[2] * state.h;
  ++state.time;
  return std::move(state);
};

smc::Model<State, Data, std::vector<double>> build_model() {
  smc::Model<State, Data, std::vector<double>> model;

  model.resampling_method = smc::multinom;

  model.rinit = [](const std::vector<double> &params) {
    State state;
    state.h = params[0];
    return state;
  };

  // Using the looped version here. Note that the looped version is called
  // rprocessN
  model.rprocessN = [](size_t n, std::vector<State> &&states,
                       const std::vector<double> &params) {
    static thread_local std::default_random_engine generator;
    auto rnorm = std::normal_distribution<double>(1.0, params[1]);
    for (size_t i = 0; i < n; ++i)
      states[i] = rprocess(std::move(states[i]), params, rnorm(generator));
    return std::move(states);
  };

  model.dmeasure = [](const Data &data, const State &state,
                      const std::vector<double> &params) {
    double ll = 0;
    if (state.x < 0 || state.h < 0 || state.h > 1)
      return log(0);
    ll = R::dpois(data.y_x, state.x, true);
    if (data.y_h >= 0) {
      ll = ll + R::dbinom(data.y_h, 5, state.h, true);
    }
    return ll;
  };

  model.rmeasure = [](const State &state, const std::vector<double> &params) {
    Data data;
    GetRNGstate();
    data.y_x = R::rpois(state.x);
    PutRNGstate();
    GetRNGstate();
    auto rnd = R::runif(0, 1);
    PutRNGstate();
    if (rnd < 0.2) {
      GetRNGstate();
      data.y_h = R::rbinom(5, state.h);
      PutRNGstate();
    }
    return data;
  };

  return model;
}

// [[Rcpp::export]]
std::vector<Data> simulate_cpp(size_t nparticles, std::vector<double> params,
                               std::vector<int> times) {
  auto model = build_model();
  model.simulate(nparticles, params, times, 0);
  auto res = model.get_measurements(nparticles, params);
  return res[0];
}

// [[Rcpp::export]]
double smc_cpp(size_t nparticles, std::vector<double> params, Rcpp::List dataR,
               std::vector<int> times, bool history = false,
               bool none = false) {
  // Convert data
  std::vector<Data> data;
  for (auto i = 0; i < dataR.size(); ++i) {
    auto dR = Rcpp::as<Rcpp::List>(dataR[i]);
    Data d;
    double dbl = dR["y_h"];
    if (std::isfinite(dbl))
      d.y_h = dbl;
    d.y_x = dR["y_x"];
    data.push_back(d);
  }

  auto model = build_model();
  if (none)
    model.resampling_method = smc::none;
  model.smc(nparticles, params, data, times, 0, history);
  return model.logLikelihood;
}

// [[Rcpp::export]]
std::vector<State> smc_result_cpp(size_t nparticles, std::vector<double> params,
                                  Rcpp::List dataR, std::vector<int> times,
                                  bool history = false) {
  // Convert data
  std::vector<Data> data;
  for (auto i = 0; i < dataR.size(); ++i) {
    auto dR = Rcpp::as<Rcpp::List>(dataR[i]);
    Data d;
    double dbl = dR["y_h"];
    if (std::isfinite(dbl))
      d.y_h = dbl;
    d.y_x = dR["y_x"];
    data.push_back(d);
  }

  auto model = build_model();
  model.smc(nparticles, params, data, times, 0, history);
  return model.get_particles(1)[0];
}

// [[Rcpp::export]]
Rcpp::List copy_and_move_count(size_t nparticles, std::vector<double> params,
                               Rcpp::List dataR, std::vector<int> times,
                               std::string resampling, bool history) {
  // Convert data
  std::vector<Data> data;
  for (auto i = 0; i < dataR.size(); ++i) {
    auto dR = Rcpp::as<Rcpp::List>(dataR[i]);
    Data d;
    double dbl = dR["y_h"];
    if (std::isfinite(dbl))
      d.y_h = dbl;
    d.y_x = dR["y_x"];
    data.push_back(d);
  }

  Rcpp::List lst(0);
  auto model = build_model();
  if (resampling == "none")
    model.resampling_method = smc::none;
  else if (resampling == "force_systematic")
    model.resampling_method = smc::force_systematic;
  else if (resampling == "force_multinom")
    model.resampling_method = smc::force_multinom;
  else  if (resampling == "multinom")
    model.resampling_method = smc::multinom;
  else  if (resampling == "systematic")
    model.resampling_method = smc::systematic;

  model.smc(nparticles, params, data, times, 0, history);
  lst["nparticles"] = model.particles.samples.size();
  // Get it from the last particle, otherwise we always get 0 copies
  lst["copy_count"] = model.particles.samples.back().copy_count;
  lst["move_count"] = model.particles.samples.back().move_count;
  size_t sum_cc = 0;
  for (size_t i = 0; i < model.particles.samples.size(); ++i) {
    sum_cc += model.particles.samples[i].copy_count;
  }
  lst["sum_copy_count"] = sum_cc;
  lst["no_times_resampled"] = model.no_times_resampled;
  return lst;
}
