// [[Rcpp::depends(RcppEigen)]]

#include <smc.hpp>

#include <atomic>

struct State {
  double h;
  double x;
};

struct Data {
  // -1 indicates that no data was available for that time step
  int y_h = -1;
  int y_x = -1;
};

namespace Rcpp {
template <> SEXP wrap(const State &data) {
  Rcpp::List RData(0);
  RData["h"] = data.h;
  RData["x"] = data.x;
  return Rcpp::wrap(RData);
}

template <> SEXP wrap(const Data &data) {
  Rcpp::List RData(0);
  RData["y_h"] = data.y_h;
  RData["y_x"] = data.y_x;
  return Rcpp::wrap(RData);
}

template <> Data as(SEXP dataRObj) {
  Data data;
  auto dR = Rcpp::as<Rcpp::List>(dataRObj);
  data.y_h = dR["y_h"];
  data.y_x = dR["y_x"];
  return data;
}
}; // namespace Rcpp

State rprocess(State &&state, const std::vector<double> &params, double hrate) {
  state.h = state.h * hrate;
  state.x = params[2] * state.h;
  return state;
};

smc::Model<State, Data, std::vector<double>> build_model() {
  smc::Model<State, Data, std::vector<double>> model;

  model.resampling_method = smc::multinom;

  model.rinit = [](const std::vector<double> &params) {
    State state;
    state.h = params[0];
    return state;
  };

  // Using the looped version here. Note that the looped version is called
  // rprocessN
  model.rprocessN = [](size_t n, std::vector<State> &&states,
                         const std::vector<double> &params) {
    static thread_local std::default_random_engine generator;
    auto rnorm = std::normal_distribution<double>(1.0, params[1]);
    for (size_t i = 0; i < n; ++i)
      states[i] = rprocess(std::move(states[i]), params, rnorm(generator));
    return std::move(states);
  };

  model.dmeasure = [](const Data &data, const State &state,
                      const std::vector<double> &params) {
    double ll = 0;
    if (state.x < 0 || state.h < 0 || state.h > 1)
      return log(0);
    ll = R::dpois(data.y_x, state.x, true);
    if (data.y_h >= 0) {
      ll = ll + R::dbinom(data.y_h, 5, state.h, true);
    }
    return ll;
  };

  model.rmeasure = [](const State &state, const std::vector<double> &params) {
    Data data;
    GetRNGstate();
    data.y_x = R::rpois(state.x);
    PutRNGstate();
    GetRNGstate();
    auto rnd = R::runif(0, 1);
    PutRNGstate();
    if (rnd < 0.2) {
      GetRNGstate();
      data.y_h = R::rbinom(5, state.h);
      PutRNGstate();
    }
    return data;
  };

  return model;
}

// [[Rcpp::export]]
std::vector<Data> simulate_cpp(size_t nparticles, std::vector<double> params,
                               std::vector<int> times) {
  auto model = build_model();
  model.simulate(nparticles, params, times, 0);
  auto res = model.get_measurements(nparticles, params);
  return res[0];
}

// [[Rcpp::export]]
double smc_cpp(size_t nparticles, std::vector<double> params, Rcpp::List dataR,
               std::vector<int> times, bool history = false,
               bool none = false) {
  // Convert data
  std::vector<Data> data;
  for (auto i = 0; i < dataR.size(); ++i) {
    auto dR = Rcpp::as<Rcpp::List>(dataR[i]);
    Data d;
    double dbl = dR["y_h"];
    if (std::isfinite(dbl))
      d.y_h = dbl;
    d.y_x = dR["y_x"];
    data.push_back(d);
  }

  auto model = build_model();
  if (none)
    model.resampling_method = smc::none;
  model.smc(nparticles, params, data, times, 0, history);
  return model.logLikelihood;
}

// [[Rcpp::export]]
std::vector<State> smc_result_cpp(size_t nparticles, std::vector<double> params,
                                  Rcpp::List dataR, std::vector<int> times,
                                  bool history = false) {
  // Convert data
  std::vector<Data> data;
  for (auto i = 0; i < dataR.size(); ++i) {
    auto dR = Rcpp::as<Rcpp::List>(dataR[i]);
    Data d;
    double dbl = dR["y_h"];
    if (std::isfinite(dbl))
      d.y_h = dbl;
    d.y_x = dR["y_x"];
    data.push_back(d);
  }

  auto model = build_model();
  model.smc(nparticles, params, data, times, 0, history);
  return model.get_particles(1)[0];
}
