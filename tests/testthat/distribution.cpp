// [[Rcpp::depends(RcppEigen)]]
#define USE_RCPP
#include <random>
#include "distribution.hpp"

std::default_random_engine generator;

// [[Rcpp::export]]
double dunif_cpp(const double value, const double lower, const double upper) {
  return distribution::dunif(value, lower, upper);
}

// [[Rcpp::export]]
double dpois_cpp(int x, double lambda) {
  return distribution::dpois(x, lambda);
}

// [[Rcpp::export]]
double dgamma_cpp(const double value, const double shape,
                     const double scale) {
  return distribution::dgamma(value, shape, scale);
}

// [[Rcpp::export]]
double dbinom_cpp(size_t k, size_t n, double p) {
  return distribution::dbinom(k, n, p);
}

// [[Rcpp::export]]
double convert_mu_to_p(double mu, double k) {
  return distribution::nbinom_convert_mu_to_p(mu, k);
}
// [[Rcpp::export]]
double dnbinom_cpp(double x, double k, double p) {
  return distribution::dnbinom(x, k, p);
}

// [[Rcpp::export]]
auto rnbinom_cpp(double k, double p) {
  return distribution::rnbinom(k, p, generator);
}

// [[Rcpp::export]]
double dbeta_cpp(double x, double alpha, double beta) {
  return distribution::dbeta(x, alpha, beta);
}

// [[Rcpp::export]]
double dbetabinomial_cpp(int k, int n, double alpha, double beta) {
  return distribution::dbetabinomial(k, n, alpha, beta);
}

// [[Rcpp::export]]
double dhypergeometric_cpp(int k, int N, int K, int n) {
  return distribution::dhypergeometric(k, N, K, n);
}

// [[Rcpp::export]]
double dlnorm_cpp(double x, double meanlog, double sdlog) {
  return distribution::dlnorm(x, meanlog, sdlog);
}
